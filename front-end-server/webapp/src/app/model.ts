export class Settings {
  constructor(
    public numstrings: string,
    public stringlength: string
  ) {}
}

export class Results {
  constructor(
    public encodedstrings: Array<string>,
  ) {}
}
