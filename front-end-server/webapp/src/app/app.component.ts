import {Component, Input, TemplateRef, OnDestroy, OnInit, ViewChild, Injectable} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatFormFieldControl } from '@angular/material';
import { FormControl, NgForm, Validators,  FormBuilder, FormGroup  } from '@angular/forms';
import {ProcessStringService} from "./processstring.service";
import {Results, Settings} from "./model";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    { provide: MatFormFieldControl, useExisting: AppComponent}   
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  displayedColumns = ["encodedstrings"];
  dataSource: Results = {encodedstrings: []};
  set : Settings = {numstrings: "0", stringlength: "0"}

  constructor(public service: ProcessStringService) {}

  private showProcessingResults(results): void {
    console.log("showProcessingResults:")
    console.log(results)
    if (results) {
      this.dataSource = results
    }
    else {
      console.log("bad results")
    }
  }

  numberOnly(event) : Boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  processInputValueChange(origin, event) {
    switch(origin) {
      case 'numstring':
        this.set.numstrings = event.toString();

      case 'stringlength':
        this.set.stringlength = event.toString();
    }
    console.log(this.set);
  }

  processStrings() {
    console.log('conditions:');
    console.log(this.set);
    if (this.set.numstrings && this.set.stringlength) {
      console.log('conditions met');
      this.service.process(this.set)
      .subscribe(result => {
        if (result !== undefined) {
            this.showProcessingResults(result)
          } else {
              console.log('Bad result');
          }
      });
    }
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }
}

