import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Results, Settings} from "./model";

@Injectable({
  providedIn: 'root'
})
export class ProcessStringService {
  baseUrl: string = '/processStrings';
  readonly headers = new HttpHeaders()
    .set('Content-Type', 'x-www-form-urlencoded');

  constructor(private http: HttpClient) {}

  process(st: Settings): Observable<Results> {
    console.log("sending ", st)
    return this.http.post<Results>(this.baseUrl, st, {headers: this.headers});
  }
}
