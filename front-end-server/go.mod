module gitlab.com/whisperer/sosiv.io/front-end-server

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.8.0
	gitlab.com/whisperer/sosiv.io/service-string-encoder v0.0.0-20211216232903-8f25d0afb629
	gitlab.com/whisperer/sosiv.io/service-string-randomizer v0.0.0-20211216232903-8f25d0afb629
	google.golang.org/grpc v1.43.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20211208223120-3a66f561d7aa // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
