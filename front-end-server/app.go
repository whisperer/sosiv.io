package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	pbencoder "gitlab.com/whisperer/sosiv.io/service-string-encoder/proto/encodestring"
	pb "gitlab.com/whisperer/sosiv.io/service-string-randomizer/proto/randomstring"

	"github.com/gorilla/mux"
	"google.golang.org/grpc"

	"github.com/rs/cors"
)

type settings struct {
	numstrings   string `json:"numstrings"`
	stringlength string `json:"stringlength"`
}

type resultStruct struct {
	Encodedstrings []string `json:"encodedstrings"`
}

type App struct {
	r *mux.Router
}

const (
	addressRandomizer = "randomizer:50051"
	addressEncoder    = "encoder:50052"
)

var wCount int = 4

const (
	HomeFolder = "/app/"
)

func HomeHandler(w http.ResponseWriter, req *http.Request) {
	http.FileServer(http.Dir(HomeFolder))
}

func (a *App) start(workerCount int) {
	wCount = workerCount
	fmt.Println("Worker count ", wCount)
	a.r.HandleFunc("/processStrings", a.ServeHTTP)
	a.r.PathPrefix("/").Handler(http.FileServer(http.Dir("/app/")))
	http.Handle("/", a.r)
	fmt.Println("Ready to serve")

	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			/* 			http.MethodPut,
			   			http.MethodPatch,
			   			http.MethodDelete,
			   			http.MethodOptions,
			   			http.MethodHead, */
		},
		AllowedHeaders: []string{
			"*",
		},
	})

	err := http.ListenAndServe(":8081", corsOpts.Handler(a.r))

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
		fmt.Printf("ListenAndServe:%sn", err.Error())
	}
}

func (a *App) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	/* if origin := req.Header.Get("Origin"); origin != "" {
		res.Header().Set("Access-Control-Allow-Origin", origin)
		res.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		res.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}

	// Stop here for a Preflighted OPTIONS request.
	if req.Method == "OPTIONS" {
		return
	} else { */

	var v interface{}
	err := json.NewDecoder(req.Body).Decode(&v)
	if err != nil {
		// handle error
	}
	log.Println(v)

	jsonStr, err := json.Marshal(v)
	if err != nil {
		fmt.Printf("Error: %s", err.Error())
	} else {
		fmt.Println("decoded1:")
		fmt.Println("`" + string(jsonStr) + "`")
	}

	numstringsStr := extractValue(string(jsonStr), "numstrings")
	stringlengthStr := extractValue(string(jsonStr), "stringlength")

	/* fmt.Println("numStrings:", numstringsStr)
	fmt.Println("stringLength:", stringlengthStr) */

	// RANDOMIZE WITHOUT GOROUTINES
	conn, err := grpc.Dial(addressRandomizer, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect: %v", err)
	}
	defer conn.Close()

	inputForRandomizerService := &pb.ServiceInput{
		Numgenstring: 0,
		StringLength: 0,
	}

	if numStrings, err := strconv.Atoi(numstringsStr); err == nil {
		fmt.Println(numStrings)
		inputForRandomizerService.Numgenstring = int32(numStrings)
	} else {
		fmt.Println(numstringsStr, "is not an integer.")
	}

	if stringlength, err := strconv.Atoi(stringlengthStr); err == nil {
		fmt.Println(stringlength)
		inputForRandomizerService.StringLength = int32(stringlength)
	} else {
		fmt.Println(stringlengthStr, "is not an integer.")
	}

	clientRandomizer := pb.NewStringRandomizerServiceClient(conn)

	responseRandomizer, err := clientRandomizer.ProcessInput(context.Background(), inputForRandomizerService)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	log.Printf("%v", responseRandomizer.Result)

	// GET GENERATED STRINGS AND FILL ARRAY FOR WORKERS
	stringsToProcess := inputForRandomizerService.Numgenstring
	stringGenerationResults := make([]string, stringsToProcess)
	for i := range stringGenerationResults {
		stringGenerationResults[i] = responseRandomizer.Result[i]
	}

	// ENCODE USING GOROUTINES
	jobs := make(chan string, stringsToProcess)
	results := make(chan string, stringsToProcess)
	/* wg := new(sync.WaitGroup) */
	//var wg sync.WaitGroup

	for w := 1; w <= wCount; w++ {
		/* wg.Add(1) */
		/* w := w */
		go workerEncoder(w, jobs, results)
	}

	for j := 1; j <= int(stringsToProcess); j++ {
		//fmt.Println("starting job ", j)
		jobs <- stringGenerationResults[j-1]
	}
	close(jobs)
	fmt.Println("all jobs done, ", stringsToProcess)
	scores := make([]string, int(stringsToProcess))
	for k := 1; k <= int(stringsToProcess); k++ {
		scores[k-1] = <-results
	}

	jsonBytes, err := StructToJSON(resultStruct{Encodedstrings: scores})
	if err != nil {
		fmt.Println("error")
		fmt.Print(err)
	}
	fmt.Println("jsonbytes:")
	fmt.Println(string(jsonBytes))

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	res.Write(jsonBytes)
	//}
	//fmt.Println("ServeHTTP")
	//a.r.ServeHTTP(res, req)
}

func workerEncoder(id int, inputstrings <-chan string, results chan<- string) {
	for j := range inputstrings {
		fmt.Println("[worker", id, " ] started job for string", j)

		conn, err := grpc.Dial(addressEncoder, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("Did not connect: %v", err)
		}
		defer conn.Close()

		inputForService := &pbencoder.ServiceInput{
			InputString: string(j),
		}

		clientEncoder := pbencoder.NewStringEncoderServiceClient(conn)

		responseEncoder, err := clientEncoder.ProcessInput(context.Background(), inputForService)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		plainString := string(responseEncoder.Result)
		log.Printf("[worker", id, " ] encoded: ", plainString)

		fmt.Println("worker", id, " ] finished job for string: ", plainString)
		results <- plainString
	}
}

func sendErr(w http.ResponseWriter, code int, message string) {
	resp, _ := json.Marshal(map[string]string{"error": message})
	http.Error(w, string(resp), code)
}

func ToSlice(c chan string) []string {
	s := make([]string, 0)
	for i := range c {
		s = append(s, i)
	}
	return s
}

func extractValue(body string, key string) string {
	keystr := "\"" + key + "\":[^,;\\]}]*"
	r, _ := regexp.Compile(keystr)
	match := r.FindString(body)
	keyValMatch := strings.Split(match, ":")
	return strings.ReplaceAll(keyValMatch[1], "\"", "")
}

func StructToJSON(data interface{}) ([]byte, error) {
	buf := new(bytes.Buffer)

	if err := json.NewEncoder(buf).Encode(data); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}
