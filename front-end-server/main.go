package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

func main() {
	workerCount := os.Getenv("WORKER_COUNT")
	app := App{
		r: mux.NewRouter(),
	}
	convertedWorkerCount, err := strconv.Atoi(workerCount)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	app.start(convertedWorkerCount)
}
