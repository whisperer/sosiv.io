package main

import (
	"fmt"
	"log"

	"context"

	pbencoder "gitlab.com/whisperer/sosiv.io/service-string-encoder/proto/encodestring"
	pb "gitlab.com/whisperer/sosiv.io/service-string-randomizer/proto/randomstring"

	"google.golang.org/grpc"
)

const (
	addressRandomizer = "localhost:50051"
	addressEncoder    = "localhost:50052"
	workerCount       = 4
)

func main() {
	// RANDOMIZE WITHOUT GOROUTINES
	conn, err := grpc.Dial(addressRandomizer, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect: %v", err)
	}
	defer conn.Close()
	fmt.Println()
	fmt.Println("Welcome to Go string generator/encoder. Please answer theese questions:")
	fmt.Println()
	fmt.Println("Number of strings: ")
	var numStrings int32
	fmt.Scanln(&numStrings)

	fmt.Println("Length of strings: ")
	var stringLength int32
	fmt.Scanln(&stringLength)

	inputForService := &pb.ServiceInput{
		Numgenstring: numStrings,
		StringLength: stringLength,
	}

	clientRandomizer := pb.NewStringRandomizerServiceClient(conn)

	responseRandomizer, err := clientRandomizer.ProcessInput(context.Background(), inputForService)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	fmt.Println("Got random generated strings:")
	log.Printf("%v", responseRandomizer.Result)

	// GET GENERATED STRINGS AND FILL ARRAY FOR WORKERS
	stringGenerationResults := make([]string, numStrings)
	for i := range stringGenerationResults {
		stringGenerationResults[i] = responseRandomizer.Result[i]
	}

	// ENCODE USING GOROUTINES
	jobs := make(chan string, numStrings)
	results := make(chan string, numStrings)

	for w := 1; w <= workerCount; w++ {
		go workerEncoder(w, jobs, results)
	}

	for j := 0; j <= int(numStrings)-1; j++ {
		jobs <- stringGenerationResults[j]
	}
	close(jobs)

	for a := 1; a <= int(numStrings); a++ {
		<-results
	}

}

func workerEncoder(id int, inputstrings <-chan string, results chan<- string) {
	for j := range inputstrings {
		fmt.Println("[worker", id, "] started job for string", j)

		conn, err := grpc.Dial(addressEncoder, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("Did not connect: %v", err)
		}
		defer conn.Close()

		inputForService := &pbencoder.ServiceInput{
			InputString: string(j),
		}

		clientEncoder := pbencoder.NewStringEncoderServiceClient(conn)

		responseEncoder, err := clientEncoder.ProcessInput(context.Background(), inputForService)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
		plainString := string(responseEncoder.Result)
		log.Println("[worker", id, "] encoding result: ", plainString)

		fmt.Println("[worker", id, "] finished job for input string", j)
		results <- plainString
	}
}
