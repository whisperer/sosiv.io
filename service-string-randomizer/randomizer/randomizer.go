package randomizer

import (
	"math/rand"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(numOfStrings int, stringLength int, charset string) []string {
	retArr := make([]string, numOfStrings)
	for i := range retArr {
		b := make([]byte, stringLength)
		for j := range b {
			b[j] = charset[seededRand.Intn(len(charset))]
		}
		retArr[i] = string(b)
	}
	return retArr
}

func GetStringArray(numOfStrings int, stringLength int) []string {
	return StringWithCharset(numOfStrings, stringLength, charset)
}
