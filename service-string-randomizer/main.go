package main

import (
	"context"
	"log"
	"net"

	pb "gitlab.com/whisperer/sosiv.io/service-string-randomizer/proto/randomstring"
	"gitlab.com/whisperer/sosiv.io/service-string-randomizer/randomizer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

type service struct {
}

func (s *service) ProcessInput(ctx context.Context, req *pb.ServiceInput) (*pb.Response, error) {

	stringArray, err := RandomizeString(req)
	if err != nil { //todo
		return nil, err
	}

	return &pb.Response{Created: true, Result: stringArray}, nil
}

func RandomizeString(input *pb.ServiceInput) ([]string, error) {
	generatedStrings := randomizer.GetStringArray(int(input.Numgenstring), int(input.StringLength))
	return generatedStrings, nil
}

func main() {

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	pb.RegisterStringRandomizerServiceServer(s, &service{})
	reflection.Register(s)

	log.Println("Running on port:", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
