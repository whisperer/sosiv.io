package main

import (
	"context"
	"log"
	"net"

	"gitlab.com/whisperer/sosiv.io/service-string-encoder/encoder"
	pb "gitlab.com/whisperer/sosiv.io/service-string-encoder/proto/encodestring"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50052"
)

type service struct {
}

func (s *service) ProcessInput(ctx context.Context, req *pb.ServiceInput) (*pb.Response, error) {

	encodedString, err := EncodeString(req)
	if err != nil { //todo maybe someday =)
		return nil, err
	}

	return &pb.Response{Encoded: true, Result: encodedString}, nil
}

func EncodeString(input *pb.ServiceInput) ([]byte, error) {
	generatedString := encoder.EncodeString(input.InputString)
	return generatedString, nil
}

func main() {

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	pb.RegisterStringEncoderServiceServer(s, &service{})
	reflection.Register(s)

	log.Println("Running on port:", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
