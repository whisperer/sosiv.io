package encoder

import (
	"crypto/sha256"
)

func EncodeString(inputString string) []byte {
	h := sha256.New()
	retVal := h.Sum([]byte(inputString))
	return retVal
}
